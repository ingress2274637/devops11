#!/bin/sh
echo "Hello, this is a sample script!"

# Load the value of APP_COLOR from the environment variable
# If not set, use a default value (e.g., "red")
APP_COLOR="${APP_COLOR:-red}"
echo "APP_COLOR is set to: $APP_COLOR"
